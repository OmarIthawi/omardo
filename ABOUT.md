#About this App

## Step #1 Start point
Although it's better for learning, setting up the project to run on both
Heroku and CircleCI would take a lot of time. So a basic Google search
shows some boilerplate projects.

I've picked one and pushed it to my BitBucket account, I have several followers
on GitHub and TravisCI so I wanted to have a bit more privacy.


## Step #2 Initial fixes
Written README.md and fixed few issues, added basic skeleton to test
Heroku push.


## Step #3 TDD
Write a test for the needed feature and implement it to fix that failing test.
Wash, rinse and repeat till the features are there!

Well, I couldn't allocate enough time for this project. Had to skip major
features and TDD.
