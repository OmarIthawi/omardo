import logging
import requests


logger = logging.getLogger(__name__)


class DealClient(object):
    BASE_URL = 'https://offersvc.expedia.com/offers/v2/getOffers'
    BASE_PARAMS = {
        'scenario': 'deal-finder',
        'page': 'foo',
        'uid': 'foo',
        'productType': 'Hotel',
    }

    def clean(self, search_results):
        return search_results

    def get(self, search_params):
        params = self.BASE_PARAMS.copy()
        params.update(search_params)
        res = requests.get(url=self.BASE_URL, params=params)
        logger.info('Got results for: %s', res.url)
        return self.clean(res.json())
