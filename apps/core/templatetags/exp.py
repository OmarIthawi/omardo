from django import template
from urllib import unquote

register = template.Library()


@register.filter
def unquote_raw(value):
    return unquote(value)


@register.inclusion_tag('stars.html')
def stars(rating):
    rating = float(rating)

    full_count = int(rating)
    icons = ['star'] * full_count

    if 0.5 == (rating - full_count):
        icons.append('star-half-o')

    empty_count = 5 - len(icons)

    icons.extend(['star-o'] * empty_count)

    assert len(icons) == 5, 'Something went wrong with our stellar system!'

    return {
        'rating': rating,
        'icons': icons,
    }
