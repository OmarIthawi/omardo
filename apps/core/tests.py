from django.core.urlresolvers import reverse
from django.conf import settings
from django.test import TestCase, override_settings, Client, RequestFactory


class SmokeTestIndexView(TestCase):
    def test_smoke_test_index(self):
        self.client.get('/')

    def test_form_exists(self):
        res = self.client.get('/')
        self.assertContains(res, '<form',
                            msg_prefix='The form should be displayed')
