from django import forms


class DealFormSerializer(object):
    def serialize(self, form):
        return {
            'destinationName': form.cleaned_data['name'],
            'minStarRating': form.cleaned_data['star_rating'],
        }


class DealForm(forms.Form):
    STAR_RATING_CHOICES = [
        ['1', 'Any rating'],
        ['2', '2 stars and more'],
        ['3', '3 stars and more'],
        ['4', '4 and 5 stars'],
        ['5', '5 stars'],
    ]

    name = forms.CharField(
        label='Destination',
        max_length=100,
        required=False,
    )

    star_rating = forms.ChoiceField(
        label='Hotel Class',
        choices=STAR_RATING_CHOICES,
        required=False,
    )
