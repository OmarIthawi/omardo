from django.views.generic import TemplateView

import json

from apps.core.forms import DealForm, DealFormSerializer
from apps.core.apiclients import DealClient


class IndexView(TemplateView):
    template_name = 'index.html'
    form_class = DealForm

    def get_context_data(self, **kwargs):
        context_data = super(IndexView, self).get_context_data(**kwargs)

        form = self.form_class(self.request.GET or None)

        if self.request.GET and form.is_valid():
            client = DealClient()
            serializer = DealFormSerializer()
            search_results = client.get(serializer.serialize(form))
            context_data['search_results'] = search_results
            context_data['search_results_dump'] = json.dumps(
                search_results,
                indent=2
            )

        context_data['form'] = form
        return context_data
