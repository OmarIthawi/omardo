# Install
Install Heroku on your machine (I'm using Ubuntu 16.04):
<https://devcenter.heroku.com/articles/heroku-cli>.

```
$ sudo apt-get install virtualenvwrapper
$ mkvirtualenv exp
$ sudo apt-get install postgresql-common postgresql-server-dev-9.5
$ pip install -r requirements.txt
$ touch .env
$ heroku local:run python manage.py migrate
```

# Develop
Do your changes and then run:
```
$ workon exp
$ heroku local:run python manage.py test
```

# Run
```
$ workon exp
$ heroku local web
```
